<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kcethsw');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('FS_METHOD', 'direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'H|4-Y9-G/{DAT/YP6(|t. R-w`+/Z%Py1QUF0IPh-Ld5c|%1`/*[Z-_irn+V:ZO#');
define('SECURE_AUTH_KEY',  'VhDA&D%pG>A:m`F|.W+)|Mh#.^U+|0[S[59z`4a.)X4EAz>:={#$:bV|wQz-1.+t');
define('LOGGED_IN_KEY',    ']%-qPX.,LNY-lRe#.@:#w`yatoqoVWh3q}A</,S`)vnbTN^Dj#bN~#T*G5(%Q^>!');
define('NONCE_KEY',        '^@Ce]]t`1a_kw|64`|S#~c ko-:t{VAxG#su#A=~VJl0+H+o47LkRU4g7?P _p4}');
define('AUTH_SALT',        'l{HMehdNgK;02ln`-/(,Ar)J9R>-)C12X4>Cu<%IZ2zj6z`Tl^>Er`1m)D#LmU{H');
define('SECURE_AUTH_SALT', 'iDeXli+EkwT!*/jSS{e54hA-91f_ec[]Gd( 8yr-mZ|bJfk&KJ%b}+=;tjA}4}=S');
define('LOGGED_IN_SALT',   'CG(*vCR%sjZ+vk]r7t`->Bs3rdp5Dg>=1^~YqDwUWDKoqdz4XWS-PdgnW3EIPm_`');
define('NONCE_SALT',       'Fi-}1O>3G^$Q`@XUp>OC?jV!,a3vR@F8d%6TiST}FO}>h/XA3 xP-&~%ey$?c>Ii');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

